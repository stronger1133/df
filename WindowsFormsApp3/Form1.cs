﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

/**
 * 2020.07.28 
 * @author 정석진
 * C# CRUD Winform
 */

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        MySqlConnection con;
        MySqlDataAdapter adap;
        DataSet ds;
        MySqlCommandBuilder cmdb1;     //기본 변수선언

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                con = new MySqlConnection();
                con.ConnectionString = "Data Source=localhost; port = 3306; username = root; Password = 1234";   //연결정보 
                con.Open();  //DB연결
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        } // DB연결
         
        private void button1_Click(object sender, EventArgs e)
        {
            adap = new MySqlDataAdapter("select * from test1.dept2", con);
            ds = new System.Data.DataSet();
            adap.Fill(ds, "dbtest");
            dataGridView1.DataSource = ds.Tables[0];
        } //조회문

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                cmdb1 = new MySqlCommandBuilder(adap);
                adap.Update(ds, "dbtest");
                MessageBox.Show("입력완료");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        } //입력문

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                cmdb1 = new MySqlCommandBuilder(adap);
                adap.Update(ds, "dbtest");
                MessageBox.Show("수정완료");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        } // 수정문

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlCommand sqlcmd = new MySqlCommand();
                sqlcmd.Connection = con;
                sqlcmd.CommandText = "delete from test1.dept2 where dnum=" + dataGridView1.Rows[selecteddnum].Cells[0].Value;
                sqlcmd.ExecuteNonQuery();
                dataGridView1.Rows.RemoveAt(selecteddnum);
                MessageBox.Show("삭제완료");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        } // 삭제문


        int selecteddnum = 0; // 삭제를 위한 변수선언

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            selecteddnum = e.RowIndex; //삭제를 위한 그리드뷰 이벤트처리
        }
    }
}
